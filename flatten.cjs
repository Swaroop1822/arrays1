function flatten(array, depth) {
    if (!depth && depth !== 0) {
        depth = 1;
    }
    let flatarray = [];
    for (let index = 0; index < array.length; index++) {
        let value = array[index]
        if (Array.isArray(array[index])) {
            if (depth > 1) {
                flatarray.push(...flatten(array[index], depth - 1));
            }
            else {
                let index = 0, len = value.length;
                while (index < len) {
                    if (value[index] !== undefined) {
                        flatarray.push(value[index]);}
                        index++;
                    }
                }
        }else if(array[index] !== undefined)
        flatarray.push(array[index]);
        
    }return flatarray;
}


module.exports = flatten;